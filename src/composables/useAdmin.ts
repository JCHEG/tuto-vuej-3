import Ressource from "@/interfaces/ressourceInterface";
import useRessourceStore from "@/stores/ressourceStore";

const useAdmin = () => {
  const ressourceStore = useRessourceStore();
  const deleteRessourceAction = (ressource: Ressource) => {
    ressourceStore.deleteRessource(ressource);
  };

  const validateRessource = (ressource: Ressource) => {
    ressource.isValid = true;
    ressourceStore.updateRessource(ressource);
  };

  return { ressourceStore, deleteRessourceAction, validateRessource };
};

export default useAdmin;
