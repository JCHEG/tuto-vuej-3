interface Ressource {
  id?: string;
  media: "post" | "video" | "book";
  title: string;
  lang: "fr" | "en";
  image?: string;
  url: string;
  description: string;
  rating: number;
  isTop: boolean;
  isValid: boolean;
  date: string;
}

export default Ressource;
