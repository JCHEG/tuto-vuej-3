import Ressource from "@/interfaces/ressourceInterface";
import { defineStore } from "pinia";
import ressourceService from "@/services/ressourceService";

const useRessourceStore = defineStore("RessourceStore", {
  state: () => ({ ressources: [] as Ressource[] }),
  getters: {
    invalidRessources: (state) =>
      state.ressources.filter((ressource) => !ressource.isValid),
  },
  actions: {
    async loadRessources() {
      const results = await ressourceService.getRessources();
      this.ressources = results;
    },
    async addRessource(ressource : Ressource) : Promise<Ressource | false> {
      const addedRessource = await ressourceService.postRessource(ressource);
      if (addedRessource) {
        this.ressources.unshift(addedRessource)
        return addedRessource;
      } else {
        return false;
      }
    },
    async deleteRessource (ressourceToDelete : Ressource){
      const result = await ressourceService.deleteRessource(ressourceToDelete)
      if (result) {
        this.ressources = this.ressources.filter(
          (ressource) => ressource.id !== ressourceToDelete.id
        );
      }
    },
    async updateRessource (ressourceToValidate : Ressource) :  Promise<Ressource | false> {
      const updatedRessource = await ressourceService.updateRessource(
        ressourceToValidate
      );

      if (updatedRessource) {
        const index = this.ressources.indexOf(ressourceToValidate);
        this.ressources[index] = ressourceToValidate;
        return ressourceToValidate;
      } else {
        return false;
      }
    },
  },
});

export default useRessourceStore;
