import User from "@/interfaces/userInterface";
import { defineStore } from "pinia";
import axios from "axios";
import credentialI from "@/interfaces/credentialInterface"

const useAuthStore = defineStore("AuthStore", {
  //permet de persister ce store dans le local storage
  persist: true,
  state: () => ({ user: {} as User }),
  getters: {
    getUser: (state) => {
      state.user
    },
    headers : (state) => {
      return { headers: { Authorization: `Bearer ${state.user.accessToken}` } };
    }

  },
  actions: {
    async connect(credentials: credentialI): Promise<boolean> {
      try {
        const results = await axios.post(
          `${import.meta.env.VITE_SERVER_API}/signin`,
          credentials
        );

        console.log(results.data);

        this.user.email = results.data.user.email;
        this.user.accessToken = results.data.accessToken;

        return true;
      } catch (error) {
        return false;
      }
    },
  },
});

export default useAuthStore;
