import { createRouter, createWebHistory,createWebHashHistory, RouteRecordRaw } from "vue-router";
import Home from "@/views/Home.vue";
import Ressource from "@/views/Ressource.vue";
import Login from "@/views/Login.vue";
import useAuthStore from "@/stores/authStore";

const routes: RouteRecordRaw[] = [
  {
    path: "/",
    component: Home,
    name: "Home",
  },
  {
    path: "/login",
    component: Login,
    name: "Login",
  },
  {
    path: "/about",
    // lazy loading, ce composant sera donc chargé quand l'utilisateur demandera la vue About
    component: () => import("@/views/About.vue"),
    name: "About",
  },
  {
    path: "/ressource/:id",
    component:Ressource,
    name: "Ressource",
  },
  {
    path: "/admin",
    // lazy loading, ce composant sera donc chargé quand l'utilisateur demandera la vue About
    component: () => import("@/views/Admin.vue"),
    name: "Admin",
    meta: { needAuth: true },
    // Les routes imbriquée sont utiles pour sous menu ou des onglets
    children: [
      {
        path: "management",
        // lazy loading, ce composant sera donc chargé quand l'utilisateur demandera la vue About
        component: () => import("@/views/admin/Management.vue"),
        name: "Management",
      },
      {
        path: "validation",
        // lazy loading, ce composant sera donc chargé quand l'utilisateur demandera la vue About
        component: () => import("@/views/admin/Validation.vue"),
        name: "Validation",
      },
    ]
  },
];

//avec history: createWebHistory() on a pas de # dans l'URL mais on ne peut pas partager les URL
const router = createRouter({ history: createWebHistory(), routes });

// ci-dessou avec history: createWebHashHistory() on a un # dans l'URL et on ne peut les URL
// const router = createRouter({ history: createWebHashHistory(), routes });


//NUXT EST UN FRAMEWORK QUI ENCAPSULE VUEJS ET QUI FAIT DU SERVER SIDE RENDERING ET IL GERE DONC LES URL POUR LE REFERENCEMENT GOOGLE

router.beforeEach((to, from, next) => {
  const authStore = useAuthStore();
  const isAuthenticated = authStore.user.accessToken || false;
  const isProtected = to.matched.some(route => route.meta.needAuth);
  if (!isAuthenticated && isProtected) {
    next({ name : 'Login' });
  }else{
    next();
  }
});

export default router;
