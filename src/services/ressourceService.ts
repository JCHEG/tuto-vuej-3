import axios from "axios";
import ressourceInterface from "@/interfaces/ressourceInterface";
import Ressource from "@/interfaces/ressourceInterface";
import useAuthStore from "@/stores/authStore";


const ressourceService = {
  async getRessources() : Promise<ressourceInterface[]> {
    try {
      const results = await axios.get(
        `${import.meta.env.VITE_SERVER_API}/ressources?_sort=date&_order=desc`
      );
      return results.data;
    } catch (e) {
      console.log(e);
      return [];
    }
  },
  async getRessourceByID(id : string) : Promise<ressourceInterface | false> {
    try {
      const results = await axios.get(
        `${import.meta.env.VITE_SERVER_API}/ressources/${id}`
      );
      console.log("ID envoyée : ", id);
      return results.data;
    } catch (e) {
      console.log(e);
      return false;
    }
  },
  async postRessource(ressource : ressourceInterface) : Promise<ressourceInterface | false> {
    try {
      const results = await axios.post(
    `${import.meta.env.VITE_SERVER_API}/ressources?_`,
        ressource
      );
      return results.data;
    } catch (e) {
      return false;
    }
  },
  async deleteRessource(ressource : Ressource) : Promise<boolean> {
    const authStore = useAuthStore();
    try {
      await axios.delete(`${import.meta.env.VITE_SERVER_API_PROTECTED}/ressources/${ressource.id}`, authStore.headers)
      return true
    } catch(e) {
      console.error(e)
      return false
    }
  },
  async updateRessource(ressource : Ressource) : Promise<boolean> {
    const authStore = useAuthStore();
    try {
      await axios.put(`${import.meta.env.VITE_SERVER_API_PROTECTED}/ressources/${ressource.id}`,
        ressource,
        authStore.headers
      );
      return true
    } catch(e) {
      console.error(e)
      return false
    }
  },
};

export default ressourceService;
