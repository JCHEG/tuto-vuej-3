/**
 * main.ts
 *
 * Bootstraps Vuetify and other plugins then mounts the App`
 */

// Components
import App from './App.vue'

// Composables
import { createApp } from 'vue'

// Plugins
import { registerPlugins } from '@/plugins'

//Router
import router from "@/router";

import '@/assets/main.css'

import {createPinia} from 'pinia'

import piniaPluginPersistedstate from "pinia-plugin-persistedstate";

const app = createApp(App)

app.use(router);

const pinia = createPinia();
// ajout du pluggin qui permet de  persister les store dans le local storage
pinia.use(piniaPluginPersistedstate);

app.use(pinia);

registerPlugins(app)

app.mount('#app')
