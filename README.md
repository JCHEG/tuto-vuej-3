# Tuto cours Vue JS 3
##Chaque commit de ce repo représente une notion distinct de vue JS

## Ressource est une application qui propose la mise a1 disposition de ressources (livre. video, ou post) avec un espace admin pour valider ou invalider les ressources

## Project setup

```
# yarn
yarn

# npm
npm install

# pnpm
pnpm install
```

### Compiles and hot-reloads for development

```
# yarn
yarn dev

# npm
npm run dev

# pnpm
pnpm dev
```

### Compiles and minifies for production

```
# yarn
yarn build

# npm
npm run build

# pnpm
pnpm build
```

### Customize configuration

See [Configuration Reference](https://vitejs.dev/config/).
